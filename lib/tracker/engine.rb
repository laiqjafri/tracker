require 'less-rails'
require 'twitter-bootstrap-rails'
require 'jquery-rails'
require 'nokogiri'
require 'tracker/shipment_tracker'
require 'mechanize'

module Tracker
  class Engine < ::Rails::Engine
    isolate_namespace Tracker
    initializer 'Load Helpers Hook' do |app|
      ActiveSupport.on_load :action_controller do
        helper Tracker::TrackHelper
      end
    end
  end
end

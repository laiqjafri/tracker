require 'nokogiri'
require 'open-uri'
require 'mechanize'
require 'json'

module Tracker
  module ShipmentTracker
    def self.track_shipment(tracking_number)
        history = self.track_dhl_shipment(tracking_number)
        unless history[:success]
          history = self.track_fedex_shipment(tracking_number)
        end
        return history
    end

    def self.track_dhl_shipment(tracking_number)
      company = ""
      success = false
      events = []
      status = ""
      dhl_url = "http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=en&idc=#{tracking_number}&extendedSearch=true"
      doc = Nokogiri::HTML(open(dhl_url))

      dhl_events = doc.search('#events table tr')
      if dhl_events.length > 0
        company = "DHL"
        success = true
        dhl_events.each do |dhl_event|
          event = []
          event_parts = dhl_event.search('td')
          event_parts.each do |event_part|
            event << event_part.content.strip
          end
          events << event
        end
        status = events.last.last
      end

      return {
        :success => success,
        :company => company,
        :events  => events,
        :status  => status
      }
    end

    def self.track_fedex_shipment(tracking_number)
      company = ""
      success = false
      events = []
      status = ""

      r = Mechanize.new
      r.verify_mode = OpenSSL::SSL::VERIFY_NONE
      response = r.get("https://www.fedex.com/trackingCal/track?data={%22TrackPackagesRequest%22%3A{%22appType%22%3A%22wtrk%22%2C%22uniqueKey%22%3A%22%22%2C%22processingParameters%22%3A{%22anonymousTransaction%22%3Atrue%2C%22clientId%22%3A%22WTRK%22%2C%22returnDetailedErrors%22%3Atrue%2C%22returnLocalizedDateTime%22%3Afalse}%2C%22trackingInfoList%22%3A[{%22trackNumberInfo%22%3A{%22trackingNumber%22%3A%22#{tracking_number}%22%2C%22trackingQualifier%22%3A%22%22%2C%22trackingCarrier%22%3A%22%22}}]}}&action=trackpackages&locale=en_US&format=json&version=99").body
      response = JSON.parse response
      if response["TrackPackagesResponse"]["successful"]
        company = "FedEx"
        success = true
        fedex_events = response["TrackPackagesResponse"]["packageList"][0]["scanEventList"]
        status = fedex_events[0]["status"]
        fedex_events.each do |event|
          events << [event["date"] + " " + event["time"], event["status"], event["scanLocation"]]
        end
      end

      return {
        :success => success,
        :company => company,
        :events  => events,
        :status  => status
      }
    end
  end
end

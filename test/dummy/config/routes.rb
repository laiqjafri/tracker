Rails.application.routes.draw do
  mount Tracker::Engine => "/tracker", :as => "tracker"
  root 'shipments#index'
  resources :shipments
end

$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "tracker/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "Tracker"
  s.version     = Tracker::VERSION
  s.authors     = ["Laiq Jafri"]
  s.email       = ["laiq.jafri@gmail.com"]
  s.homepage    = "https://bitbucket.org/laiqjafri/tracker"
  s.summary     = "Shipments Tracker"
  s.description = "Tracks shipments for DHL and Fedex"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.2"
  s.add_dependency "therubyracer"
  s.add_dependency "less-rails"
  s.add_dependency "twitter-bootstrap-rails"
  s.add_dependency "jquery-rails"
  s.add_dependency "nokogiri", "~> 1.6.1"
  s.add_dependency "mechanize"

  s.add_development_dependency "mysql2"
end

module Tracker
  module TrackHelper
    def track_my_shipment_link(tracking_number = false)
      if tracking_number
        link_to "Track #{tracking_number}", tracker.track_shipment_url(tracking_number), {:class => 'modal-link'}
      else
        link_to 'Track your order', tracker.form_track_index_url, {:class => 'modal-link'}
      end
    end
  end
end

require_dependency "tracker/application_controller"

module Tracker
  class TrackController < ApplicationController
    def shipment
      tracking_number = params[:track_id]
      @history = Tracker::ShipmentTracker::track_shipment(tracking_number)
    end

    def form
    end
  end
end

$(document).ready(function() {
  var show_processing = function() {
    var processing_div_html = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">' +
  '<div class="modal-header">' +
  '<h1>Processing...</h1>' +
  '</div>' +
  '<div class="modal-body">' +
  '<div class="progress progress-striped active">' +
  '<div class="bar" style="width: 100%;"></div>' +
  '</div>' +
  '</div>' +
  '</div>');

    processing_div_html.modal();
  };

  $(".modal-link").unbind('click');
  $(".modal-link").click(function(e) {
    e.preventDefault();
    var link = $(this);
    var url = link.attr("href");
    var data = link.data();
    link.attr("disabled", true);
    show_processing();
    $.ajax({
      url: url,
      data: data,
      success: function(html) {
        var processing_modal = $(".modal");
        var processing_bd_modal = $(".modal-backdrop");
        $("<div class=\"modal hide fade\">" + html + "</div>").modal();
        processing_modal.hide('slow');
        processing_bd_modal.hide('slow');
        link.attr("disabled", false);
      },
      error: function(html) {
        link.attr("disabled", false);
      }
    });
  });

  $(".track-shipment-form").unbind('submit');
  $(".track-shipment-form").submit(function(e) {
    e.preventDefault();
    var tracking_number = $('#tracker-tracking-number').val().trim();
    if(tracking_number == "") {
      $('#tracker-error-message').show();
      $('#tracker-error-message').html("Please enter upto 30 digits as your tracking number.");
      return false;
    }

    if(!$.isNumeric(tracking_number)) {
      $('#tracker-error-message').show();
      $('#tracker-error-message').html("Tracking number should only contain digits");
      return false;
    }

    show_processing();
    $.ajax({
      url: '/tracker/track/' + tracking_number + '/shipment',
      data: {},
      success: function(html) {
        var processing_modal = $(".modal");
        var processing_bd_modal = $(".modal-backdrop");
        $("<div class=\"modal hide fade\">" + html + "</div>").modal();
        processing_modal.hide('slow');
        processing_bd_modal.hide('slow');
      },
      error: function(html) {
      }
    });
  });
});

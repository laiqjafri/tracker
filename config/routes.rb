Tracker::Engine.routes.draw do
  root :to => 'track#shipment'
  resources :track, :only => [] do
    get 'shipment'
    collection do
      get  'form'
    end
  end
end
